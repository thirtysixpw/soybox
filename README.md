# SoyBox Hugo Theme

A simple Hugo theme I plan on using for my personal website and website for my school based on [lugo](https://github.com/LukeSmithxyz/lugo).

## Get started

```sh
hugo new site new-site
cd new-site
git clone https://codeberg.org/thirtysixpw/soybox themes/soybox
echo "theme = 'soybox'" >> config.toml
cp themes/soybox/static/style.css static/
```

## Stuff

- Makes one RSS feed for the entire site at `/index.xml`
- Stylesheet is in `/style.css` and includes some important stuff for partials.
- If a post is tagged, links to the tags are placed at the bottom of the post.
- `nextprev.html` adds links to the Next and Previous articles to the bottom of a page.
- `taglist.html` links all tags an article is tagged to for related content.
- By default, the home index has its own custom file because I don't like it being a list. Remove `/layouts/index.html` if you want your home to function like a list as is default in Hugo.
